<?php
/**
 * WooCommerce Integration. SKU Generator.
 *
 * @since      1.0
 * @package    WPDesk\SKU
 */

namespace WPDesk\SKUGenerator;

use SKUGeneratorVendor\WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Class Generator
 *
 * @package WPDesk\SKU
 */
class Generator implements Hookable {

	/**
	 * SKU Prefix.
	 *
	 * @var string
	 */
	private $sku_prefix;

	/**
	 * WooCommerceSku constructor.
	 */
	public function __construct() {
		$this->init_settings();
	}

	/**
	 * Init & fires hooks.
	 */
	public function hooks() {
		add_filter( 'woocommerce_inventory_settings', [ $this, 'add_inventory_fields' ] );
		add_action( 'save_post', array( $this, 'update_sku_metadata_action' ), 2, 10 );
	}

	/**
	 * Init settings.
	 */
	private function init_settings() {
		$generate_prefix = substr( str_shuffle( 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ), 0, 5 );
		$sku_prefix      = get_option( 'woocommerce_sku_prefix', null );
		if ( ! $sku_prefix || empty( $sku_prefix ) ) {
			update_option( 'woocommerce_sku_prefix', $generate_prefix );
			update_option( 'woocommerce_sku_format', 1 );
		}
	}

	/**
	 * Add SKU fields to the inventory section.
	 *
	 * @param array $settings Settings.
	 *
	 * @return array
	 */
	public function add_inventory_fields( $settings ) {

		$settings[] = array(
			'title' => __( 'SKU Generator', 'woocommerce-sku-generator' ),
			'type'  => 'title',
		);

		$settings[] = array(
			'title'    => __( 'Prefix', 'woocommerce-sku-generator' ),
			'type'     => 'text',
			'default'  => $this->sku_prefix,
			'id'       => 'woocommerce_sku_prefix',
			'desc_tip' => __( 'Set own prefix or leave default.', 'woocommerce-sku-generator' ),
		);

		$settings[] = array(
			'title'    => __( 'Format', 'woocommerce-sku-generator' ),
			'type'     => 'select',
			'id'       => 'woocommerce_sku_format',
			'desc_tip' => __( 'Select the SKU format.', 'woocommerce-sku-generator' ),
			'options'  => [
				'1' => __( 'Prefix & Product ID [SKU_1]', 'woocommerce-sku-generator' ),
				'2' => __( 'Prefix & Product ID & Category ID [SKU_1_10]', 'woocommerce-sku-generator' ),
				'3' => __( 'Prefix & Product ID & Time [SKU_1_10_00]', 'woocommerce-sku-generator' ),
			],
		);

		$settings[] = array(
			'title'   => __( 'Dash separator', 'woocommerce-sku-generator' ),
			'type'    => 'checkbox',
			'id'      => 'woocommerce_sku_separator_dash',
			'desc'    => __( 'Use dash as separator instead of underscore.', 'woocommerce-sku-generator' ),
			'default' => 'no',
		);

		$settings[] = array(
			'title'   => __( 'Remove separator', 'woocommerce-sku-generator' ),
			'type'    => 'checkbox',
			'id'      => 'woocommerce_sku_separator_remove',
			'desc'    => __( 'Do not use separator between data.', 'woocommerce-sku-generator' ),
			'default' => 'no',
		);

		$settings[] = array(
			'type' => 'sectionend',
			'id'   => 'product_inventory_options',
		);

		return $settings;
	}

	/**
	 * Save generated SKU.
	 *
	 * @param int      $post_id Post ID.
	 * @param \WP_Post $post    Post.
	 *
	 * @return string|bool
	 */
	public function update_sku_metadata_action( $post_id, \WP_Post $post ) {

		if ( 'product' !== get_post_type( $post_id ) ) {
			return false;
		}

		$sku = $this->generate_sku( $post_id );
		if ( empty( get_post_meta( $post_id, '_sku', true ) ) ) {
			update_post_meta( $post_id, '_sku', $sku );
		}

		return false;
	}

	/**
	 * Generate SKU.
	 *
	 * @param int $post_id Post ID.
	 *
	 * @return string
	 */
	private function generate_sku( $post_id ) {
		$terms   = get_the_terms( $post_id, 'product_cat' );
		$term_id = 0;
		if ( ! is_wp_error( $terms ) && isset( $terms[0]->term_id ) ) {
			$term_id = $terms[0]->term_id;
		}

		$prefix           = get_option( 'woocommerce_sku_prefix', '' );
		$format           = get_option( 'woocommerce_sku_format', '1' );
		$separator        = get_option( 'woocommerce_sku_separator_dash', 'no' );
		$remove_separator = get_option( 'woocommerce_sku_separator_remove', 'no' );
		$glue             = 'no' === $separator ? '_' : '-';

		if ( 'yes' === $remove_separator ) {
			$glue = '';
		}

		if ( ! $prefix ) {
			return '';
		}

		switch ( $format ) {
			case '1':
				$sku = $prefix . $glue . $post_id;
				break;
			case '2':
				$sku = $prefix . $glue . $post_id . $glue . $term_id;
				break;
			case '3':
				$sku = $prefix . $glue . $term_id . $glue . date( 'H_i', strtotime( 'NOW' ) );
				break;
			default:
				$sku = '';
		}

		return $sku;
	}

}

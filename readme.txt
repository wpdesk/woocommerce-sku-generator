=== WooCommerce SKU Generator ===

Contributors: wpdesk,dyszczo,grola,potreb
Donate link: https://www.wpdesk.net/
Tags: woocommerce, sku, generator
Requires at least: 4.5
Tested up to: 5.3.2
Requires PHP: 5.6
Stable tag: 1.2.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Generate SKU for new WooCommerce products.

== Description ==

Automatically generate a SKU for parent / simple products, variations, or both when the product is published or updated.



Includes Polish and German translations and can be easily translated to other languages.

> **Get more WooCommerce plugins from WP Desk**<br />
> We provide premium plugins for customizing checkout, shipping, invoicing and more. Check out our [premium WooCommerce plugins here →](https://www.wpdesk.net/products/?utm_source=wporg&utm_medium=link&utm_campaign=woocartweight)

= Other Free WooCommerce Plugins =

Make sure to check our great plugin [Flexible Shipping for WooCommerce](https://wordpress.org/plugins/flexible-shipping/). Create virtually any shipping scenario based on totals, weight, quantity, shipping classes and more.

== Installation	 ==

You can install this plugin like any other WordPress plugin.

1. Download and unzip the latest release zip file.
2. Upload the entire plugin directory to your /wp-content/plugins/ directory.
3. Activate the plugin through the Plugins menu in WordPress Administration.

You can also use WordPress uploader to upload plugin zip file in menu Plugins -> Add New -> Upload Plugin. Then go directly to point 3.

== Screenshots ==

1. SKU settings.
2. Product edit.

== Changelog ==

= 1.0 - 2015.08.11 =
* First Release!

#!/bin/bash

export WPDESK_PLUGIN_SLUG=woocommerce-sku-generator
export WPDESK_PLUGIN_TITLE="WooCommerce SKU Generator"

export WOOTESTS_IP=${WOOTESTS_IP:wootests}

sh ./vendor/wpdesk/wp-codeception/scripts/common_bootstrap.sh
